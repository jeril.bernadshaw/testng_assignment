package com.test;

import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class NewTest3 {
  @Test(groups={"Smoke Test"})
  public void createCustomer() {
	  System.out.println("The customer will be created");
  }
  @Test(groups={"Regression Test"})
  public void NewCustomer() {
	  System.out.println("New Customer created");
  }
  @Test(groups={"Usability Test"})
  public void ModifyCustomer() {
	  System.out.println("Customer will get modified");
  }
  @Test(groups={"Smoke Test"})
  public void ChangeCustomer() {
	  System.out.println("Customer changed");
	  
  }
  @BeforeClass
  public void beforeClass() {
	  System.out.println("Launch browser");
  }
  @AfterClass
  public void afterClass() {
	  System.out.println("Close Browser");
  }
}
	