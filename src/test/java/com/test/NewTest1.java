package com.test;

import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class NewTest1 {
  @Test(priority=1)
  public void modifyCustomer() {
	  System.out.println("The Customer will get modified");
  }
  @Test(priority = 3)
  public void createCustomer() {
	  System.out.println("The Customer will get created");
  }
  @Test(priority = 2)
  public void newCustomer() {
	  System.out.println("New Customer will get created");
  }
  
  @BeforeMethod//before every test cases
  public void beforeCustomer() {
	  System.out.println("Verfying the customer"); 
  }
  @AfterMethod//after every test case
  public void afterCustomer() {
	  System.out.println("All the transactions are Done");
  }
  @BeforeClass//start time at once
  public void beforeClass() {
	  System.out.println("Start databse connection, launch browser");
}
  @AfterClass//End time at once
  public void afterClass() {
	  System.out.println("Close database connection, close browser");
}
}